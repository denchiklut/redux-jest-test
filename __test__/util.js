import reducers from "../src/reducers"
import { createStore, applyMiddleware } from "redux"
import checkPropTypes from 'check-prop-types'
import { middlewares } from "../src"

export const storeFactory = (initialState) => {
    const cswm = applyMiddleware(...middlewares)(createStore)
    return cswm(reducers, initialState)
}

export const findByTest = (wrapper, val) => wrapper.find(`[data-test='${ val }']`)

export const checkProps = (component, props) => {
    const propError = checkPropTypes(
        component.propTypes,
        props,
        'prop',
        component.name)
    expect(propError).toBeUndefined()
}

