import { storeFactory } from "../../__test__/util";
import { guessWord } from "../actions";

describe('guessWord action dispatcher', () => {
    const secretWord = 'party'
    const unsuccessfulGuess = 'train'
    const initialState = { secretWord }
    let store

    beforeEach(() => {
        store = storeFactory(initialState)
    });

    it('update state correctly for unsuccessful guess', () => {
        store.dispatch(guessWord(unsuccessfulGuess))
        const newState = store.getState()
        const expectedState = {
            ...initialState,
            success: false,
            guessedWords: [{ guessedWord: unsuccessfulGuess, letterMatchedCount: 3 }]
        }

        expect(newState).toEqual(expectedState)
    });

    it('update state correctly for successful guess', () => {
        store.dispatch(guessWord(secretWord))
        const newState = store.getState()
        const expectedState = {
            ...initialState,
            success: true,
            guessedWords: [{ guessedWord: secretWord, letterMatchedCount: 5 }]
        }

        expect(newState).toEqual(expectedState)
    });
});
