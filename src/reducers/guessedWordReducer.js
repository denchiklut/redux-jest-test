import { ACTIONS } from "../actions";

export default (state = [], action) => {
    switch (action.type) {
        case ACTIONS.GUESS_WORD: return [...state, action.payload]
        default: return state
    }

}
