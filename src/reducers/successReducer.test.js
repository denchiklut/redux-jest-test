import { ACTIONS } from "../actions"
import successReducer from "./successReducer"

describe('successReducer', () => {
    it('returns `false` when no action is past', () => {
        const newState = successReducer(undefined,{ type: '' })
        expect(newState).toBe(false)
    });

    it('returns `true` when no action type is `CORRECT_GUESS`', () => {
        const newState = successReducer(undefined,{ type: ACTIONS.CORRECT_GUESS })
        expect(newState).toBe(true)
    });
});
