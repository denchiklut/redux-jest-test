import { ACTIONS } from "../actions"

export default (state = false, action) => {
    switch (action.type) {
        case ACTIONS.CORRECT_GUESS: return true
        default: return state
    }
}
