import {ACTIONS} from "../actions";

export default (state = '', action) => {
    switch (action.type) {
        case ACTIONS.SET_SECRET_WORD: return action.payload
        default: return state
    }
}
