import React from 'react'
import ReactDOM from 'react-dom'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from "redux"

import App from './components/App'
import reducers from "./reducers"

export const middlewares = [thunk]
const store = createStore(reducers, applyMiddleware(...middlewares))

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root') || document.createElement('div') // for testing purposes
)
