import { getLetterMatchCount } from "./index"

describe('getLetterMatchCount', () => {
    const secret = 'party'
    it('return correct count when nothing match ', () => {
        const letterMatchCount = getLetterMatchCount('bones', secret)
        expect(letterMatchCount).toBe(0)
    });

    it('return correct count when 3 match ', () => {
        const letterMatchCount = getLetterMatchCount('train', secret)
        expect(letterMatchCount).toBe(3)
    });

    it('return correct count when duplicated letters match ', () => {
        const letterMatchCount = getLetterMatchCount('parka', secret)
        expect(letterMatchCount).toBe(3)
    });
});
