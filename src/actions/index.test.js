import moxios from 'moxios'
import { storeFactory } from "../../__test__/util"
import { getSecretWord } from "./index"

describe('Actions tests', () => {
    beforeEach(() => {
       moxios.install()
    });

    afterEach(() => {
        moxios.uninstall()
    });

    it('it `getSecretWord` actionCreator test', () => {
        const secretWord = 'party'
        const store = storeFactory()

        moxios.wait(() => {
            const request = moxios.requests.mostRecent()
            request.respondWith({
                status: 200,
                response: secretWord
            })
        })

        return store.dispatch(getSecretWord())
            .then(() => {
                const newState = store.getState()
                expect(newState.secretWord).toBe(secretWord)
            })
    });
});
