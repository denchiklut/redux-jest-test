import React from 'react'
import { shallow } from 'enzyme'
import Input, { UnconnectedInput } from "./Input"
import { findByTest, storeFactory } from "../../../__test__/util"

const setup = (initiaalState={ }) => {
    const store = storeFactory(initiaalState)
    return  shallow(<Input store={ store } />).dive().dive()
}

describe('<Input />', () => {
    it('render without crashes', () => {
        const wrapper = setup({ success: true })
        const component = findByTest(wrapper, 'Input')

        expect(component.length).toBe(1)
    })

    it('render input box', () => {
        const wrapper = setup({ success: false })
        const inputBox = findByTest(wrapper, 'input-box')

        expect(inputBox.length).toBe(1)
    })

    it('render submit btn', () => {
        const wrapper = setup({ success: false })
        const submit = findByTest(wrapper, 'submit')

        expect(submit.length).toBe(1)
    })

    it('not render Input`s form when `success` is false', () => {
        const wrapper = setup({ success: true })

        const submit = findByTest(wrapper, 'submit')
        const inputBox = findByTest(wrapper, 'input-box')
        const component = findByTest(wrapper, 'Input')

        expect(component.length).toBe(1)
        expect(submit.length).toBe(0)
        expect(inputBox.length).toBe(0)
    })

    it('has `success` piece of state as prop', () => {
        const success = true
        const wrapper = setup({ success })

        const successProp = wrapper.instance().props.success
        expect(successProp).toBe(success)
    })

    it('`guessWord` actionCreator is a func prop', () => {
        const wrapper = setup()
        const guessWordProp = wrapper.instance().props.guessWord

        expect(guessWordProp).toBeInstanceOf(Function)
    })

    it('`guessWord` runs on submit with correct value', () => {
        const guessWordMock = jest.fn()
        const guessedWord = 'train'
        const props = { success: false, guessWord: guessWordMock }
        const wrapper = shallow(<UnconnectedInput {...props} />)

        wrapper.instance().inputBox.current = { value: guessedWord }

        const btn = findByTest(wrapper, 'submit')
        btn.simulate('click', { preventDefault: jest.fn() })

        const guessWordCallArgs = guessWordMock.mock.calls[0][0]
        expect(guessWordCallArgs).toBe(guessedWord)
    })

})
