import React, { Component } from 'react'
import { connect } from "react-redux"
import { guessWord } from "../../actions"

export class UnconnectedInput extends Component {

    inputBox = React.createRef()

    submitGuessWord = (e) => {
        e.preventDefault()

        const guessedWord = this.inputBox.current.value
        if (guessedWord && guessedWord.length > 0) {
            this.props.guessWord(guessedWord)
        }
    }

    render() {
        let content
        if (!this.props.success) {
            content = (
                <form className='ui fluid action input' onSubmit={ this.submitGuessWord }>
                    <input
                        type="text"
                        placeholder="Word..."
                        data-test='input-box'
                        ref = { this.inputBox }
                    />
                    <button
                        className="ui button"
                        type='submit'
                        data-test='submit'
                        onClick={ this.submitGuessWord }
                    >
                        submit
                    </button>
                </form>

            )
        }

        return (
            <div className="ui container" data-test='Input'>
                { content }
            </div>
        )
    }
}

const mapStateToProps = ({ success }) => {
    return { success }
}

export default connect( mapStateToProps, { guessWord } )(UnconnectedInput)
