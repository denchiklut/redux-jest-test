import React from 'react'
import PropTypes from "prop-types"

const GuessedWords = ({ guessedWords }) => {
    let content

    if (guessedWords.length === 0) {
        content = (
            <span data-test='guess-instructions'>
                Try to guess the secret word!
            </span>
        )
    } else {
        const guessedWordsRow = guessedWords.map((word, idx) => (
            <tr key={idx} data-test='guessed-word'>
                <td className='warning'>{ word.guessedWord }</td>
                <td>{ word.letterMatchedCount }</td>
            </tr>
        ))

        content = (
            <div data-test='guessed-words'>
                <h2 className="ui center aligned icon header">
                    <img className="ui image" src="https://semantic-ui.com/images/icons/school.png" />
                        <div className="content">
                            Guessed words
                        </div>
                </h2>
                <div className="ui container">
                    <table className='ui table'>
                        <thead>
                        <tr>
                            <th>Guessed</th>
                            <th>Matching letters</th>
                        </tr>
                        </thead>

                        <tbody>
                        { guessedWordsRow }
                        </tbody>
                    </table>
                </div>

            </div>
        )
    }

    return (<div className='ui container' data-test='GuessedWords'>{ content }</div>)
}

GuessedWords.propTypes = {
    guessedWords: PropTypes.arrayOf(
        PropTypes.shape({
            guessedWord: PropTypes.string.isRequired,
            letterMatchedCount: PropTypes.number.isRequired
        })
    ).isRequired
}

export default GuessedWords
