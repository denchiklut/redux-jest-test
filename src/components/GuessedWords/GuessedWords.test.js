import React from 'react'
import { shallow } from 'enzyme'
import GuessedWords from "./GuessedWords"
import {checkProps, findByTest} from "../../../__test__/util";

const defaultProps = {
    guessedWords: [{ guessedWord: 'train', letterMatchedCount: 3 }]
}

const setup = (props) => {
    const setupProps = { ...defaultProps, ...props }
    return shallow(<GuessedWords { ...setupProps } />)
}

describe('<GuessedWords />', () => {
    it('check default props', () => {
       checkProps(GuessedWords, defaultProps)
    });

    it('render withou crashes', () => {
        const wrapper = setup()
        const component = findByTest(wrapper, 'GuessedWords')

        expect(component.length).toBe(1)
    });

    it('render instructions', () => {
        const wrapper = setup({ guessedWords: [] })
        const component = findByTest(wrapper, 'guess-instructions')

        expect(component.text().length).not.toBe(0)
    });

    it('render guessed word section', () => {
        const props = {
            guessedWords: [
                { guessedWord: 'train', letterMatchedCount: 3 },
                { guessedWord: 'agile', letterMatchedCount: 1 },
                { guessedWord: 'party', letterMatchedCount: 5 },
            ]
        }
        const wrapper = setup(props)
        const guessedWordNode = findByTest(wrapper, 'guessed-words')

        expect(guessedWordNode.length).toBe(1)

    });

    it('correct number of guessed words', () => {
        const props = {
            guessedWords: [
                { guessedWord: 'train', letterMatchedCount: 3 },
                { guessedWord: 'agile', letterMatchedCount: 1 },
                { guessedWord: 'party', letterMatchedCount: 5 },
            ]
        }
        const wrapper = setup(props)
        const guessedWordsNode = findByTest(wrapper, 'guessed-word')

        expect(guessedWordsNode.length).toBe(props.guessedWords.length)
    });
});
