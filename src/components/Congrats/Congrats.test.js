import React from 'react'
import { shallow } from 'enzyme/build'
import { checkProps, findByTest } from "../../../__test__/util"
import Congrats from './Congrats'

const defaultProps = { success: false }

const setup = (props={}) => {
    const setupProps = { ...defaultProps, ...props }
    return shallow(<Congrats {...setupProps}/>)
}

describe('<Congrats />', () => {
    it('render no msg when props `success`success` is false', () => {
        const wrapper = setup()
        const component = findByTest(wrapper, 'congrats')

        expect(component.length).toBe(0)
    });

    it('render congrats msg when props `success` is true', () => {
        const wrapper = setup({ success: true })
        const component = findByTest(wrapper, 'congrats')

        expect(component.text()).not.toBe(0)
    });

    it('checks propTypes', () => {
        const expectedProps = { success: true }
        checkProps(Congrats, expectedProps)
    });
});
