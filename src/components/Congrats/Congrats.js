import React from 'react'
import PropTypes from 'prop-types'

const Congrats = ({ success }) => {
    if (success) return (
        <div className="ignored info ui message container" data-test='congrats'>
            congratulations
        </div>
    )
    return null
}

Congrats.propTypes = {
    success: PropTypes.bool.isRequired
}

export default Congrats
