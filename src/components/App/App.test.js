import React from 'react'
import Enzyme, { shallow } from 'enzyme/build'
import EnzymeAdapter from 'enzyme-adapter-react-16/build'
import App, { UnConnectedApp } from './App'
import { findByTest, storeFactory } from "../../../__test__/util"

Enzyme.configure({ adapter: new EnzymeAdapter() })

const setup = (state={}) => {
  const store = storeFactory(state)
  return shallow(<App store={ store }/>).dive().dive()
}

describe('<App />', () => {
  it('render without crashes', () => {
    const wrapper = setup()
    const component = findByTest(wrapper, 'App')

    expect(component.length).toBe(1)
  });

  it('has access to `success` state', () => {
    const success = true
    const wrapper = setup({ success })
    const successProp = wrapper.instance().props.success

    expect(successProp).toBe(success)
  })

  it('has access to `guessedWords` state', () => {
    const guessedWords = [{ guessedWord: 'train', letterMatchedCount: 3 }]
    const wrapper = setup({ guessedWords })
    const guessedWordsProp = wrapper.instance().props.guessedWords

    expect(guessedWordsProp).toEqual(guessedWords)
  })

  it('has access to `secretWord` state', () => {
    const secretWord = 'party'
    const wrapper = setup({ secretWord })
    const secretWordProp = wrapper.instance().props.secretWord

    expect(secretWordProp).toBe(secretWord)
  })

  it('`getSecretWord` actionCreator as a prop', () => {
    const wrapper = setup()
    const getSecretWordProp = wrapper.instance().props.getSecretWord

    expect(getSecretWordProp).toBeInstanceOf(Function)
  })

  it('`getSecretWord` runs on App mount', () => {
    const getSecretWordMock = jest.fn()
    const props = { success: false, guessedWords: [], getSecretWord: getSecretWordMock }
    const wrapper = shallow(<UnConnectedApp {...props} />)
    wrapper.instance().componentDidMount()

    const getSecretWordCallCount = getSecretWordMock.mock.calls.length
    expect(getSecretWordCallCount).toBe(1)
  });
})


