import React, { Component } from 'react'
import {connect} from "react-redux"
import Input from "../Input/Input";
import GuessedWords from "../GuessedWords"
import Congrats from "../Congrats"
import { getSecretWord } from "../../actions"

export class UnConnectedApp extends Component {
    componentDidMount() {
        this.props.getSecretWord()
    }

    render() {
      const { success, secretWord, guessedWords } = this.props
      return (
          <div className="App" data-test="App" >
              <h2 className="ui center aligned segment">
                  Jotto
              </h2>
              <Congrats success={ success } />
              <Input />
              <GuessedWords guessedWords={ guessedWords } />
          </div>
      );
    }
  }

const mapStateToProps = ({ success, secretWord, guessedWords }) => {
    return { success, secretWord, guessedWords }
}

export default connect(mapStateToProps, { getSecretWord })(UnConnectedApp)
